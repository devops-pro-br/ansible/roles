# Roles Ansible 

Repositório contendo roles Ansible para variados casos de uso.

## 1. Lista de roles
 - commom: ações comuns a todos os hosts
 - docker: instala o docker
## 2. Como usar

### 2.1. Ajustar iventário

Ajustar o arquivo de inventário [inventory-global.yml](inventory-global.yml) conforme a necessidade ou utilizar algum plugin de inventário dinâmico.

### 2.2. Definir Roles

Através do arquivo [playbook.yml](playbook.yml) é possível definir quais roles serão executadas. Pode-se apenas comentar as que não quer executar.

### 2.3. Execução

Para executar:

```shell
ansible-playbook playbook.yml
```

Para realizar apenas um check, sem executar:

```shell
ansible-playbook playbook.yml --check
```

Para validar quais hosts serão alcançados, sem executar:

```shell
ansible-playbook playbook.yml --list-hosts
```

